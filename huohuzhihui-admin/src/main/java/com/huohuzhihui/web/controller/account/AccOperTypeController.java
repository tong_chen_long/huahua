package com.huohuzhihui.web.controller.account;

import com.huohuzhihui.account.domain.AccOperType;
import com.huohuzhihui.account.service.IAccOperTypeService;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账户操作类型Controller
 * 
 * @author huohuzhihui
 * @date 2021-08-25
 */
@RestController
@RequestMapping("/account/opertype")
public class AccOperTypeController extends BaseController
{
    @Autowired
    private IAccOperTypeService accOperTypeService;

    /**
     * 查询账户操作类型列表
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:list')")
    @GetMapping("/list")
    public TableDataInfo list(AccOperType accOperType)
    {
        startPage();
        List<AccOperType> list = accOperTypeService.selectAccOperTypeList(accOperType);
        return getDataTable(list);
    }

    /**
     * 导出账户操作类型列表
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:export')")
    @Log(title = "账户操作类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AccOperType accOperType)
    {
        List<AccOperType> list = accOperTypeService.selectAccOperTypeList(accOperType);
        ExcelUtil<AccOperType> util = new ExcelUtil<AccOperType>(AccOperType.class);
        return util.exportExcel(list, "opertype");
    }

    /**
     * 获取账户操作类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(accOperTypeService.selectAccOperTypeById(id));
    }

    /**
     * 新增账户操作类型
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:add')")
    @Log(title = "账户操作类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AccOperType accOperType)
    {
        return toAjax(accOperTypeService.insertAccOperType(accOperType));
    }

    /**
     * 修改账户操作类型
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:edit')")
    @Log(title = "账户操作类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AccOperType accOperType)
    {
        return toAjax(accOperTypeService.updateAccOperType(accOperType));
    }

    /**
     * 删除账户操作类型
     */
    @PreAuthorize("@ss.hasPermi('account:opertype:remove')")
    @Log(title = "账户操作类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accOperTypeService.deleteAccOperTypeByIds(ids));
    }
}
