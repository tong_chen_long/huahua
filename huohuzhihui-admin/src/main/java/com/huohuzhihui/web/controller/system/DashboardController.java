package com.huohuzhihui.web.controller.system;

import com.huohuzhihui.account.domain.AccRecharge;
import com.huohuzhihui.account.service.IAccCardService;
import com.huohuzhihui.account.service.IAccRechargeService;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.account.vo.AccRechargeChartVo;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.entity.Account;
import com.huohuzhihui.common.core.domain.entity.Card;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.merchant.domain.MerCost;
import com.huohuzhihui.merchant.service.IMerCostService;
import com.huohuzhihui.merchant.vo.MerCostChartVo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 看板
 */
@RestController
@RequestMapping("/system/dashboard")
public class DashboardController extends BaseController {
    @Autowired
    private IAccUserAccountService accUserAccountService;

    @Autowired
    private IAccCardService accCardService;

    @Autowired
    private IAccRechargeService accRechargeService;
    @Autowired
    private IMerCostService merCostService;



    /**
     * 看板
     */
    @GetMapping("/init")
    public Map<String,Object> init()
    {
        Map<String,Object> result = new HashMap<>();
        Date today = DateUtils.initDateByDay();//今日
        Date tomorrow = DateUtils.addDays(today,1);//明日
        Date yestoday = DateUtils.addDays(today,-1);//昨日
        //获取账户信息
        Account account = new Account();
        account.setStatus(0);
        account.setCreateTime(today);
        //获取当日新增用户数
        Long addUserCount = accUserAccountService.getUserAccountStatistics(account);
        account.setStatus(2);
        account.setCreateTime(null);
        account.setUpdateTime(today);
        //获取当日注销用户数
        Long cancelUserCount = accUserAccountService.getUserAccountStatistics(account);
        //当日补卡人数
        Card accCard = new Card();
        accCard.setOperType(1);
        accCard.setCreateTime(today);
        Long replaceUserCount = accCardService.getCardtStatistics(accCard);

        //今日消费
        MerCost merCost = new MerCost();
        merCost.setBeginTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",today));
        merCost.setEndTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",tomorrow));
        BigDecimal totalCost = merCostService.getTotalAmount(merCost);
        //昨日消费
        merCost.setBeginTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",yestoday));
        merCost.setEndTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",today));
        BigDecimal yestodayTotalCost = merCostService.getTotalAmount(merCost);
        //今日充值
        AccRecharge accRecharge = new AccRecharge();
        accRecharge.setBeginTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",today));
        accRecharge.setEndTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",tomorrow));
        BigDecimal totalRecharge = accRechargeService.getTotalAmount(accRecharge);
        //昨日充值
        accRecharge.setBeginTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",yestoday));
        accRecharge.setEndTime(DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",today));
        BigDecimal yestodayTotalRecharge = accRechargeService.getTotalAmount(accRecharge);
        //账户余额
        BigDecimal totalUserBalance = accUserAccountService.getTotalUserBalance();
        totalUserBalance = totalUserBalance == null ? BigDecimal.ZERO : totalUserBalance;
        //账户余额
        result.put("totalUserBalance",totalUserBalance);
        //当日新增人数
        result.put("addUserCount",addUserCount);
        //当日补卡人数
        result.put("cancelUserCount",cancelUserCount);
        //当日注销人数
        result.put("replaceUserCount",replaceUserCount);
        //昨日消费
        result.put("yestodayTotalCost",yestodayTotalCost);
        //今日消费
        result.put("totalCost",totalCost);
        //昨日充值
        result.put("yestodayTotalRecharge",yestodayTotalRecharge);
        //今日充值
        result.put("totalRecharge",totalRecharge);
        return result;
    }

    /**
     * 当月消费统计折线图数据
     */
    @GetMapping("/getChart")
    public Map<String,Object> getChart()
    {
        Map<String,Object> result = new HashMap<>();
        //当月天数
        int days = DateUtils.getCurrentMonthDay();
        //获取当月充值订单
        List<AccRechargeChartVo> rechargeChartVoList = accRechargeService.getCurrentMothOrder();
        //获取当月消费订单
        List<MerCostChartVo> merCostChartVoList = merCostService.getCurrentMothOrder();
        result.put("addOrderList",matchData(rechargeChartVoList,days));
        result.put("reduceOrderList",matchCostData(merCostChartVoList,days));
        logger.info("充值金额"+result.get("addOrderList"));
        logger.info("消费金额"+result.get("reduceOrderList"));
        return result;
    }

    private List<BigDecimal> matchCostData(List<MerCostChartVo> reduceOrderList, int days) {
        if(CollectionUtils.isNotEmpty(reduceOrderList)){
            Map<Integer,MerCostChartVo> map = reduceOrderList.stream().collect(Collectors.toMap(MerCostChartVo::getDayName, orderChart->orderChart ));
            for (int i = 1; i <= days; i++) {
                //如果没有当天数据，手动插入为0的数据
                if(!map.containsKey(i)){
                    MerCostChartVo chart = new MerCostChartVo();
                    chart.setDayName(i);
                    chart.setAmount(BigDecimal.ZERO);
                    reduceOrderList.add(chart);
                }
            }
        }else {
            reduceOrderList = new ArrayList<>();
            for (int i = 1; i <= days; i++) {
                //如果没有当天数据，手动插入为0的数据
                MerCostChartVo chart = new MerCostChartVo();
                chart.setDayName(i);
                chart.setAmount(BigDecimal.ZERO);
                reduceOrderList.add(chart);
            }
        }
        return reduceOrderList.stream().sorted(Comparator.comparing(MerCostChartVo::getDayName)).map(e -> e.getAmount()).collect(Collectors.toList());

    }

    /**
     * 匹配数据
     * @param addOrderList
     * @param days
     */
    private List<BigDecimal> matchData(List<AccRechargeChartVo> addOrderList ,int days){
        if(CollectionUtils.isNotEmpty(addOrderList)){
            Map<Integer,AccRechargeChartVo> map = addOrderList.stream().collect(Collectors.toMap(AccRechargeChartVo::getDayName, orderChart->orderChart ));
            for (int i = 1; i <= days; i++) {
                //如果没有当天数据，手动插入为0的数据
                if(!map.containsKey(i)){
                    AccRechargeChartVo chart = new AccRechargeChartVo();
                    chart.setDayName(i);
                    chart.setAmount(BigDecimal.ZERO);
                    addOrderList.add(chart);
                }
            }
        }else {
            addOrderList = new ArrayList<>();
            for (int i = 1; i <= days; i++) {
                //如果没有当天数据，手动插入为0的数据
                AccRechargeChartVo chart = new AccRechargeChartVo();
                chart.setDayName(i);
                chart.setAmount(BigDecimal.ZERO);
                addOrderList.add(chart);
            }
        }
        return addOrderList.stream().sorted(Comparator.comparing(AccRechargeChartVo::getDayName)).map(e -> e.getAmount()).collect(Collectors.toList());
    }

}
