import request from '@/utils/request'

// 查询门禁列表
export function listGuard(query) {
  return request({
    url: '/oa/guard/list',
    method: 'get',
    params: query
  })
}

// 查询门禁详细
export function getGuard(id) {
  return request({
    url: '/oa/guard/' + id,
    method: 'get'
  })
}

// 新增门禁
export function addGuard(data) {
  return request({
    url: '/oa/guard',
    method: 'post',
    data: data
  })
}

// 修改门禁
export function updateGuard(data) {
  return request({
    url: '/oa/guard',
    method: 'put',
    data: data
  })
}

// 删除门禁
export function delGuard(id) {
  return request({
    url: '/oa/guard/' + id,
    method: 'delete'
  })
}

// 导出门禁
export function exportGuard(query) {
  return request({
    url: '/oa/guard/export',
    method: 'get',
    params: query
  })
}