import request from '@/utils/request'

// 查询出入记录列表
export function listInout(query) {
  return request({
    url: '/oa/inout/list',
    method: 'get',
    params: query
  })
}

// 查询出入记录详细
export function getInout(id) {
  return request({
    url: '/oa/inout/' + id,
    method: 'get'
  })
}

// 新增出入记录
export function addInout(data) {
  return request({
    url: '/oa/inout',
    method: 'post',
    data: data
  })
}

// 修改出入记录
export function updateInout(data) {
  return request({
    url: '/oa/inout',
    method: 'put',
    data: data
  })
}

// 删除出入记录
export function delInout(id) {
  return request({
    url: '/oa/inout/' + id,
    method: 'delete'
  })
}

// 导出出入记录
export function exportInout(query) {
  return request({
    url: '/oa/inout/export',
    method: 'get',
    params: query
  })
}
// 下载导入模板
export function importTemplate() {
  return request({
    url: '/oa/inout/importTemplate',
    method: 'get'
  })
}