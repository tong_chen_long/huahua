import request from '@/utils/request'

// 查询会议申请列表
export function listMeeting(query) {
  return request({
    url: '/oa/meeting/list',
    method: 'get',
    params: query
  })
}


// 查询个人会议申请列表
export function myMeeting(query) {
  return request({
    url: '/oa/meeting/my',
    method: 'get',
    params: query
  })
}

// 查询会议申请详细
export function getMeeting(id) {
  return request({
    url: '/oa/meeting/' + id,
    method: 'get'
  })
}

// 新增会议申请
export function addMeeting(data) {
  return request({
    url: '/oa/meeting',
    method: 'post',
    data: data
  })
}

// 修改会议申请
export function updateMeeting(data) {
  return request({
    url: '/oa/meeting',
    method: 'put',
    data: data
  })
}

// 删除会议申请
export function delMeeting(id) {
  return request({
    url: '/oa/meeting/' + id,
    method: 'delete'
  })
}

// 导出会议申请
export function exportMeeting(query) {
  return request({
    url: '/oa/meeting/export',
    method: 'get',
    params: query
  })
}
// 审核会议申请
export function checkMeeting(data) {
  return request({
    url: '/oa/meeting/check',
    method: 'post',
    data: data
  })
}
