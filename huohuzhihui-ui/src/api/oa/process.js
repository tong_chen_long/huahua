import request from '@/utils/request'

// 查询工作流步骤列表
export function listProcess(query) {
  return request({
    url: '/oa/process/list',
    method: 'get',
    params: query
  })
}

// 查询个人工作流步骤列表
export function myProcess(query) {
  return request({
    url: '/oa/process/my',
    method: 'get',
    params: query
  })
}

// 查询工作流步骤详细
export function getProcess(id) {
  return request({
    url: '/oa/process/' + id,
    method: 'get'
  })
}

// 新增工作流步骤
export function addProcess(data) {
  return request({
    url: '/oa/process',
    method: 'post',
    data: data
  })
}

// 修改工作流步骤
export function updateProcess(data) {
  return request({
    url: '/oa/process',
    method: 'put',
    data: data
  })
}

// 删除工作流步骤
export function delProcess(id) {
  return request({
    url: '/oa/process/' + id,
    method: 'delete'
  })
}

// 导出工作流步骤
export function exportProcess(query) {
  return request({
    url: '/oa/process/export',
    method: 'get',
    params: query
  })
}

// 签收公文
export function signin(id) {
  return request({
    url: '/oa/process/signin/' + id,
    method: 'get'
  })
}
