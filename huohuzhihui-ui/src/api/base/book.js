import request from '@/utils/request'

// 查询教材列表
export function listBook(query) {
  return request({
    url: '/base/book/list',
    method: 'get',
    params: query
  })
}


// 查询教材列表
export function selectBook(query) {
  return request({
    url: '/base/book/select',
    method: 'get',
    params: query
  })
}

// 查询教材详细
export function getBook(id) {
  return request({
    url: '/base/book/' + id,
    method: 'get'
  })
}

// 新增教材
export function addBook(data) {
  return request({
    url: '/base/book',
    method: 'post',
    data: data
  })
}

// 修改教材
export function updateBook(data) {
  return request({
    url: '/base/book',
    method: 'put',
    data: data
  })
}

// 删除教材
export function delBook(id) {
  return request({
    url: '/base/book/' + id,
    method: 'delete'
  })
}


// 启用教材
export function enableBook(id) {
  return request({
    url: '/base/book/enable/' + id,
    method: 'get'
  })
}


// 禁用教材
export function disableBook(id) {
  return request({
    url: '/base/book/disable/' + id,
    method: 'get'
  })
}

// 导出教材
export function exportBook(query) {
  return request({
    url: '/base/book/export',
    method: 'get',
    params: query
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/book/importTemplate',
    method: 'get'
  })
}