import request from '@/utils/request'

// 查询企业列表
export function listCompany(query) {
  return request({
    url: '/base/company/list',
    method: 'get',
    params: query
  })
}


// 查询企业列表
export function selectCompany(query) {
  return request({
    url: '/base/company/select',
    method: 'get',
    params: query
  })
}

// 查询企业详细
export function getCompany(id) {
  return request({
    url: '/base/company/' + id,
    method: 'get'
  })
}

// 新增企业
export function addCompany(data) {
  return request({
    url: '/base/company',
    method: 'post',
    data: data
  })
}

// 修改企业
export function updateCompany(data) {
  return request({
    url: '/base/company',
    method: 'put',
    data: data
  })
}

// 删除企业
export function delCompany(id) {
  return request({
    url: '/base/company/' + id,
    method: 'delete'
  })
}

// 导出企业
export function exportCompany(query) {
  return request({
    url: '/base/company/export',
    method: 'get',
    params: query
  })
}

// 启用企业
export function enableCompany(id) {
  return request({
    url: '/base/company/enable/' + id,
    method: 'get'
  })
}


// 禁用企业
export function disableCompany(id) {
  return request({
    url: '/base/company/disable/' + id,
    method: 'get'
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/company/importTemplate',
    method: 'get'
  })
}