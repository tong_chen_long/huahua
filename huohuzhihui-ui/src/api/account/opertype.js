import request from '@/utils/request'

// 查询账户操作类型列表
export function listOpertype(query) {
  return request({
    url: '/account/opertype/list',
    method: 'get',
    params: query
  })
}

// 查询账户操作类型详细
export function getOpertype(id) {
  return request({
    url: '/account/opertype/' + id,
    method: 'get'
  })
}

// 新增账户操作类型
export function addOpertype(data) {
  return request({
    url: '/account/opertype',
    method: 'post',
    data: data
  })
}

// 修改账户操作类型
export function updateOpertype(data) {
  return request({
    url: '/account/opertype',
    method: 'put',
    data: data
  })
}

// 删除账户操作类型
export function delOpertype(id) {
  return request({
    url: '/account/opertype/' + id,
    method: 'delete'
  })
}

// 导出账户操作类型
export function exportOpertype(query) {
  return request({
    url: '/account/opertype/export',
    method: 'get',
    params: query
  })
}