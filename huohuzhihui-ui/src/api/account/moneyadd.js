import request from '@/utils/request'

// 查询帐户增款列表
export function listMoneyadd(query) {
  return request({
    url: '/account/moneyChange/list',
    method: 'get',
    params: query
  })
}

// 查询帐户增款详细
export function getMoneyadd(id) {
  return request({
    url: '/account/moneyChange/' + id,
    method: 'get'
  })
}

// 新增帐户增款
export function addMoneyadd(data) {
  return request({
    url: '/account/moneyChange',
    method: 'post',
    data: data
  })
}

// 修改帐户增款
export function updateMoneyadd(data) {
  return request({
    url: '/account/moneyChange',
    method: 'put',
    data: data
  })
}

// 删除帐户增款
export function delMoneyadd(id) {
  return request({
    url: '/account/moneyChange/' + id,
    method: 'delete'
  })
}

// 导出帐户增款
export function exportMoneyadd(query) {
  return request({
    url: '/account/moneyChange/export',
    method: 'get',
    params: query
  })
}
