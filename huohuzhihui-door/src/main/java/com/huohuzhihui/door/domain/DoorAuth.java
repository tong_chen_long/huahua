package com.huohuzhihui.door.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.common.utils.DateUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 门禁授权对象 t_door_auth
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
public class DoorAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 门的id */
    @Excel(name = "门的id")
    private Long doorId;
    /** 门的名称 */
    private String doorName;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;
    /** 用户姓名 */
    private String nickName;
    /**手机号*/
    private String phonenumber;



    /** 授权起始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "授权起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 授权终止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "授权终止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 删除标记：0未删除，1删除 */
    private Integer delFlag;

    /** 门禁卡号：bsCardNo%(256*256) + ((int)(bsCardNo/(256*256))%256)*100000 */
    @Excel(name = "门禁卡号：bsCardNo%(256*256) + ((int)(bsCardNo/(256*256))%256)*100000")
    private String deviceCardNo;

    /** $column.columnComment */
    @Excel(name = "门禁卡号：bsCardNo%(256*256) + ((int)(bsCardNo/(256*256))%256)*100000")
    private String stuCode;

    /** 是否已写入门禁机：0，未写入，1已写入 */
    @Excel(name = "是否已写入门禁机：0，未写入，1已写入")
    private Integer isWrited;

    /** 卡号 */
    @Excel(name = "一卡通卡号")
    private Long cardNo;
    //部门授权
    private Long deptId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCardNo() {
        return cardNo;
    }

    /** 扩展权限：1-可查看门禁记录；2：可授权给他人 */
    @Excel(name = "扩展权限：1-可查看门禁记录；2：可授权给他人")
    private String authExtend;


    public void setDoorId(Long doorId)
    {
        this.doorId = doorId;
    }

    public Long getDoorId() 
    {
        return doorId;
    }


    public void setCardNo(Long cardNo)
    {
        this.cardNo = cardNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDeviceCardNo() {
        return deviceCardNo;
    }

    public void setDeviceCardNo(String deviceCardNo) {
        this.deviceCardNo = deviceCardNo;
    }

    public String getDoorName() {
        return doorName;
    }

    public void setDoorName(String doorName) {
        this.doorName = doorName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }


    public void setDelFlag(Integer delFlag)
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    public void setStuCode(String stuCode)
    {
        this.stuCode = stuCode;
    }

    public String getStuCode() 
    {
        return stuCode;
    }
    public void setIsWrited(Integer isWrited) 
    {
        this.isWrited = isWrited;
    }

    public Integer getIsWrited() 
    {
        return isWrited;
    }

    public void setAuthExtend(String authExtend) 
    {
        this.authExtend = authExtend;
    }

    public String getAuthExtend() 
    {
        return authExtend;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("doorId", getDoorId())
            .append("endTime", getEndTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .append("cardNo", getCardNo())
            .append("isWrited", getIsWrited())
            .append("stuCode", getStuCode())
            .append("isWrited", getIsWrited())
            .append("authExtend", getAuthExtend())
            .toString();
    }
}
