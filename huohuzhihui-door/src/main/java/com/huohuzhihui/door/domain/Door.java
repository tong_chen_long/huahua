package com.huohuzhihui.door.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 门禁对象 t_door
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
public class Door extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 门名称 */
    @Excel(name = "门名称")
    private String name;

    /** 删除标记 */
    private Integer delFlag;

    /** 门禁控制器id */
    @Excel(name = "门禁控制器id")
    private Long deviceId;

    /** 门禁控制器SN */
    private String deviceSn;

    /** 门禁控制器锁号 */
    private Integer lockNo;

    /** 所属分组 */
    @Excel(name = "所属分组")
    private Long doorGroupId;

    /** 所属分组名称 */
    private String doorGroupName;

    public void setId(Long id){
        this.id = id ;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }


    public void setDoorGroupId(Long doorGroupId)
    {
        this.doorGroupId = doorGroupId;
    }

    public Long getDoorGroupId() 
    {
        return doorGroupId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getLockNo() {
        return lockNo;
    }

    public void setLockNo(Integer lockNo) {
        this.lockNo = lockNo;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getDoorGroupName() {
        return doorGroupName;
    }

    public void setDoorGroupName(String doorGroupName){
        this.doorGroupName=doorGroupName;
        }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("remark", getRemark())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("delFlag", getDelFlag())
            .append("deviceId", getDeviceId())
            .append("doorGroupId", getDoorGroupId())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("lockNo", getLockNo())
            .append("doorGroupId", getDoorGroupId())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
