package com.huohuzhihui.door.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.door.domain.DoorAuth;
import com.huohuzhihui.door.service.IDoorAuthService;
import com.huohuzhihui.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 门禁授权Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/door/auth")
public class DoorAuthController extends BaseController
{
    @Autowired
    private IDoorAuthService doorAuthService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询门禁授权列表
     */
    @PreAuthorize("@ss.hasPermi('door:auth:list')")
    @GetMapping("/list")
    public TableDataInfo list(DoorAuth doorAuth)
    {
        startPage();
        List<DoorAuth> list = doorAuthService.selectDoorAuthList(doorAuth);
        return getDataTable(list);
    }

    /**
     * 导出门禁授权列表
     */
    @PreAuthorize("@ss.hasPermi('door:auth:export')")
    @Log(title = "门禁授权", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DoorAuth doorAuth)
    {
        List<DoorAuth> list = doorAuthService.selectDoorAuthList(doorAuth);
        ExcelUtil<DoorAuth> util = new ExcelUtil<DoorAuth>(DoorAuth.class);
        return util.exportExcel(list, "auth");
    }

    /**
     * 获取门禁授权详细信息
     */
    @PreAuthorize("@ss.hasPermi('door:auth:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(doorAuthService.selectDoorAuthById(id));
    }

    /**
     * 新增门禁授权
     */
    @PreAuthorize("@ss.hasPermi('door:auth:add')")
    @Log(title = "门禁授权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DoorAuth doorAuth)
    {
        doorAuth.setCreateBy(SecurityUtils.getUsername());
        return toAjax(doorAuthService.insertDoorAuth(doorAuth));
    }

    @PreAuthorize("@ss.hasPermi('door:auth:add')")
    @Log(title = "门禁授权", businessType = BusinessType.INSERT)
    @PostMapping("/addDeptAuth")
    public AjaxResult addDeptAuth(@RequestBody DoorAuth doorAuth)
    {
        Long deptId = doorAuth.getDeptId();
        //根据部门查询人员
        SysUser user = new SysUser();
        user.setDeptId(deptId);
        List<SysUser> userList = this.sysUserService.selectUserList(user);
        int count = 0;
        if(userList!=null && userList.size()>0){
            for(int i = 0 ; i < userList.size(); i++){
                DoorAuth auth = new DoorAuth();
                auth.setDoorId(doorAuth.getDoorId());
                auth.setUserId(userList.get(i).getUserId());
                auth.setStartDate(doorAuth.getStartDate());
                auth.setEndDate(doorAuth.getEndDate());
                auth.setCreateBy(SecurityUtils.getUsername());
                count+=doorAuthService.insertDoorAuth(auth);
            }
        }
        return toAjax(count);
    }

    /**
     * 同步授权记录到门禁设备
     * @param id
     * @return
     */
    @PostMapping("/write2device")
    public AjaxResult write2device(Long id)
    {
        try{
            this.doorAuthService.writeAuth(doorAuthService.selectDoorAuthById(id));
            return AjaxResult.success("同步门禁设备中进出记录成功");
        }catch (CustomException e){
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }

    }



    /**
     * 修改门禁授权
     */
    @PreAuthorize("@ss.hasPermi('door:auth:edit')")
    @Log(title = "门禁授权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DoorAuth doorAuth)
    {
        doorAuth.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(doorAuthService.updateDoorAuth(doorAuth));
    }

    /**
     * 删除门禁授权
     */
    @PreAuthorize("@ss.hasPermi('door:auth:remove')")
    @Log(title = "门禁授权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(doorAuthService.deleteDoorAuthByIds(ids));
    }
}
