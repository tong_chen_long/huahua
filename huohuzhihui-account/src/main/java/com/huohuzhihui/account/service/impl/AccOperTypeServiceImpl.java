package com.huohuzhihui.account.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.account.mapper.AccOperTypeMapper;
import com.huohuzhihui.account.domain.AccOperType;
import com.huohuzhihui.account.service.IAccOperTypeService;

/**
 * 账户操作类型Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2021-08-25
 */
@Service
public class AccOperTypeServiceImpl implements IAccOperTypeService 
{
    @Autowired
    private AccOperTypeMapper accOperTypeMapper;

    /**
     * 查询账户操作类型
     * 
     * @param id 账户操作类型ID
     * @return 账户操作类型
     */
    @Override
    public AccOperType selectAccOperTypeById(Long id)
    {
        return accOperTypeMapper.selectAccOperTypeById(id);
    }

    /**
     * 查询账户操作类型列表
     * 
     * @param accOperType 账户操作类型
     * @return 账户操作类型
     */
    @Override
    public List<AccOperType> selectAccOperTypeList(AccOperType accOperType)
    {
        return accOperTypeMapper.selectAccOperTypeList(accOperType);
    }

    /**
     * 新增账户操作类型
     * 
     * @param accOperType 账户操作类型
     * @return 结果
     */
    @Override
    public int insertAccOperType(AccOperType accOperType)
    {
        return accOperTypeMapper.insertAccOperType(accOperType);
    }

    /**
     * 修改账户操作类型
     * 
     * @param accOperType 账户操作类型
     * @return 结果
     */
    @Override
    public int updateAccOperType(AccOperType accOperType)
    {
        return accOperTypeMapper.updateAccOperType(accOperType);
    }

    /**
     * 批量删除账户操作类型
     * 
     * @param ids 需要删除的账户操作类型ID
     * @return 结果
     */
    @Override
    public int deleteAccOperTypeByIds(Long[] ids)
    {
        return accOperTypeMapper.deleteAccOperTypeByIds(ids);
    }

    /**
     * 删除账户操作类型信息
     * 
     * @param id 账户操作类型ID
     * @return 结果
     */
    @Override
    public int deleteAccOperTypeById(Long id)
    {
        return accOperTypeMapper.deleteAccOperTypeById(id);
    }
}
