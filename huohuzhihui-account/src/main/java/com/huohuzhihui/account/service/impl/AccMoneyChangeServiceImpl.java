package com.huohuzhihui.account.service.impl;

import java.util.List;

import com.huohuzhihui.account.domain.AccOperType;
import com.huohuzhihui.account.service.IAccOperTypeService;
import com.huohuzhihui.common.constant.TradeConstants;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.account.mapper.AccMoneyChangeMapper;
import com.huohuzhihui.account.domain.AccMoneyChange;
import com.huohuzhihui.account.service.IAccMoneyChangeService;

/**
 * 帐户增款Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2021-08-11
 */
@Service
public class AccMoneyChangeServiceImpl implements IAccMoneyChangeService 
{
    @Autowired
    private AccMoneyChangeMapper accMoneyChangeMapper;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IAccOperTypeService accOperTypeService;


    /**
     * 查询帐户增款
     * 
     * @param id 帐户增款ID
     * @return 帐户增款
     */
    @Override
    public AccMoneyChange selectAccMoneyChangeById(Long id)
    {
        return accMoneyChangeMapper.selectAccMoneyChangeById(id);
    }

    /**
     * 查询帐户增款列表
     * 
     * @param accMoneyChange 帐户增款
     * @return 帐户增款
     */
    @Override
    public List<AccMoneyChange> selectAccMoneyChangeList(AccMoneyChange accMoneyChange)
    {
        return accMoneyChangeMapper.selectAccMoneyChangeList(accMoneyChange);
    }

    /**
     * 新增帐户增款
     * 
     * @param accMoneyChange 帐户增款
     * @return 结果
     */
    @Override
    public int insertAccMoneyChange(AccMoneyChange accMoneyChange)
    {
        accMoneyChange.setCreateTime(DateUtils.getNowDate());
        //修改余额
        SysUser user = sysUserService.selectUserById(accMoneyChange.getUserId());
        AccOperType accOperType = this.accOperTypeService.selectAccOperTypeById(accMoneyChange.getOperType());
        //增款
        if(accOperType.getBalanceChange()==1){
            user.setBalance(user.getBalance().add(accMoneyChange.getAmount()));
        }
        if(accOperType.getBalanceChange()==-1){
            //余额不足
            if(user.getBalance().compareTo(accMoneyChange.getAmount())==-1){
                throw new CustomException(TradeConstants.BALANCE_NOT_ENOUTH.getDesc(),TradeConstants.BALANCE_NOT_ENOUTH.getCode());
            }
            user.setBalance(user.getBalance().subtract(accMoneyChange.getAmount()));
        }
        sysUserService.updateUser(user);
        return accMoneyChangeMapper.insertAccMoneyChange(accMoneyChange);
    }

    /**
     * 修改帐户增款
     * 
     * @param accMoneyChange 帐户增款
     * @return 结果
     */
    @Override
    public int updateAccMoneyChange(AccMoneyChange accMoneyChange)
    {
        accMoneyChange.setUpdateTime(DateUtils.getNowDate());
        return accMoneyChangeMapper.updateAccMoneyChange(accMoneyChange);
    }

    /**
     * 批量删除帐户增款
     * 
     * @param ids 需要删除的帐户增款ID
     * @return 结果
     */
    @Override
    public int deleteAccMoneyChangeByIds(Long[] ids)
    {
        return accMoneyChangeMapper.deleteAccMoneyChangeByIds(ids);
    }

    /**
     * 删除帐户增款信息
     * 
     * @param id 帐户增款ID
     * @return 结果
     */
    @Override
    public int deleteAccMoneyChangeById(Long id)
    {
        return accMoneyChangeMapper.deleteAccMoneyChangeById(id);
    }
}
