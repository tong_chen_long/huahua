package com.huohuzhihui.account.service;

import com.huohuzhihui.account.vo.SubsidyVo;
import com.huohuzhihui.common.core.domain.entity.Account;

import java.math.BigDecimal;
import java.util.List;

/**
 * 账户Service接口
 * 
 * @author zylu
 * @date 2020-11-14
 */
public interface IAccUserAccountService 
{
    /**
     * 查询账户
     * 
     * @param id 账户ID
     * @return 账户
     */
    public Account selectAccUserAccountById(Long id);

    /**
     * 查询账户列表
     * 
     * @param accUserAccount 账户
     * @return 账户集合
     */
    public List<Account> selectAccUserAccountList(Account accUserAccount);

    /**
     * 新增账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    public int insertAccUserAccount(Account accUserAccount);

    /**
     * 修改账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    public int updateAccUserAccount(Account accUserAccount);

    /**
     * 批量删除账户
     * 
     * @param ids 需要删除的账户ID
     * @return 结果
     */
    public int deleteAccUserAccountByIds(Long[] ids);

    /**
     * 删除账户信息
     * 
     * @param id 账户ID
     * @return 结果
     */
    public int deleteAccUserAccountById(Long id);

    /**
     * 冻结账户
     * @param ids 需要冻结账户的ID
     * @return 结果
     */
    public int freezeUserAccountByIds(Long[] ids);

    /**
     * 解冻账户
     * @param ids
     * @return
     */
    int unfreezeUserAccountByIds(Long[] ids);

    /**
     * 注销账户
     * @param ids
     * @return
     */
    int cancelUserAccountByIds(Long[] ids);



    /**
     * 账户统计
     * @param account
     * @return
     */
    Long getUserAccountStatistics(Account account);

    /**
     * 账户总余额
     * @return
     */
    BigDecimal getTotalUserBalance();

    public Account selectAccUserAccountByUserId(Long userId);



    void updateAccUserAccountByUserId(Account accout);



}
