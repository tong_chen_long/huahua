package com.huohuzhihui.account.mapper;

import java.util.List;
import com.huohuzhihui.account.domain.AccOperType;

/**
 * 账户操作类型Mapper接口
 * 
 * @author huohuzhihui
 * @date 2021-08-25
 */
public interface AccOperTypeMapper 
{
    /**
     * 查询账户操作类型
     * 
     * @param id 账户操作类型ID
     * @return 账户操作类型
     */
    public AccOperType selectAccOperTypeById(Long id);

    /**
     * 查询账户操作类型列表
     * 
     * @param accOperType 账户操作类型
     * @return 账户操作类型集合
     */
    public List<AccOperType> selectAccOperTypeList(AccOperType accOperType);

    /**
     * 新增账户操作类型
     * 
     * @param accOperType 账户操作类型
     * @return 结果
     */
    public int insertAccOperType(AccOperType accOperType);

    /**
     * 修改账户操作类型
     * 
     * @param accOperType 账户操作类型
     * @return 结果
     */
    public int updateAccOperType(AccOperType accOperType);

    /**
     * 删除账户操作类型
     * 
     * @param id 账户操作类型ID
     * @return 结果
     */
    public int deleteAccOperTypeById(Long id);

    /**
     * 批量删除账户操作类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAccOperTypeByIds(Long[] ids);
}
