package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaField;

import java.util.List;

/**
 * 单字段Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaFieldMapper 
{
    /**
     * 查询单字段
     * 
     * @param id 单字段ID
     * @return 单字段
     */
    public OaField selectOaFieldById(Long id);

    /**
     * 查询单字段列表
     * 
     * @param oaField 单字段
     * @return 单字段集合
     */
    public List<OaField> selectOaFieldList(OaField oaField);

    /**
     * 新增单字段
     * 
     * @param oaField 单字段
     * @return 结果
     */
    public int insertOaField(OaField oaField);

    /**
     * 修改单字段
     * 
     * @param oaField 单字段
     * @return 结果
     */
    public int updateOaField(OaField oaField);

    /**
     * 删除单字段
     * 
     * @param id 单字段ID
     * @return 结果
     */
    public int deleteOaFieldById(Long id);

    /**
     * 批量删除单字段
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaFieldByIds(Long[] ids);
}
