package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaInout;

import java.util.List;

/**
 * 出入记录Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-10
 */
public interface OaInoutMapper 
{
    /**
     * 查询出入记录
     * 
     * @param id 出入记录ID
     * @return 出入记录
     */
    public OaInout selectOaInoutById(Long id);

    /**
     * 查询出入记录列表
     * 
     * @param oaInout 出入记录
     * @return 出入记录集合
     */
    public List<OaInout> selectOaInoutList(OaInout oaInout);

    /**
     * 新增出入记录
     * 
     * @param oaInout 出入记录
     * @return 结果
     */
    public int insertOaInout(OaInout oaInout);

    /**
     * 修改出入记录
     * 
     * @param oaInout 出入记录
     * @return 结果
     */
    public int updateOaInout(OaInout oaInout);

    /**
     * 删除出入记录
     * 
     * @param id 出入记录ID
     * @return 结果
     */
    public int deleteOaInoutById(Long id);

    /**
     * 批量删除出入记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaInoutByIds(Long[] ids);
}
