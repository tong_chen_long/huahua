package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 单字段对象 oa_field
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("单字段实体")
@Getter
@Setter
public class OaField extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 工作流 */
    @Excel(name = "工作流")
    @ApiModelProperty("工作流")
    private Long flowId;

    /** 字段代码 */
    @Excel(name = "字段代码")
    @ApiModelProperty("字段代码")
    private String code;

    /** 字段名称 */
    @Excel(name = "字段名称")
    @ApiModelProperty("字段名称")
    private String title;

    /** 字段类型 */
    @Excel(name = "字段类型")
    @ApiModelProperty("字段类型")
    private String type;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty("排序")
    private Long listSort;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty("状态")
    private String status;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flowId", getFlowId())
            .append("code", getCode())
            .append("title", getTitle())
            .append("type", getType())
            .append("listSort", getListSort())
            .append("status", getStatus())
            .toString();
    }
}
