package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 工作流步骤对象 oa_flow_step
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("工作流步骤实体")
@Getter
@Setter
public class OaFlowStep extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 工作流 */
    @Excel(name = "工作流")
    @ApiModelProperty("工作流")
    private Long flowId;

    /** 上一步 */
    @ApiModelProperty("上一步")
    private Long pid;
    
    @Excel(name = "上一步")
    private String pname;

    /** 步骤名称 */
    @Excel(name = "步骤名称")
    @ApiModelProperty("步骤名称")
    private String title;

    /** 类型 */
    @Excel(name = "类型", dictType = "step_type")
    @ApiModelProperty("类型")
    private String type;

    /** 规则 */
    @Excel(name = "规则", dictType = "step_rule")
    private String rule;
    
    /** 参数 */
    @Excel(name = "参数")
    private String param;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flowId", getFlowId())
            .append("pid", getPid())
            .append("title", getTitle())
            .append("type", getType())
            .append("rule", getRule())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
