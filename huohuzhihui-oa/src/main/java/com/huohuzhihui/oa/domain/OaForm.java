package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 公文单对象 oa_form
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("公文单实体")
@Getter
@Setter
public class OaForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 公文 */
    @Excel(name = "公文")
    @ApiModelProperty("公文")
    private Long documentId;

    /** 流程 */
    @Excel(name = "流程")
    @ApiModelProperty("流程")
    private Long flowId;

    /** 字段 */
    @ApiModelProperty("字段")
    private Long fieldId;
    
    @Excel(name = "字段")
    private OaField field;


    /** 内容 */
    @Excel(name = "内容")
    @ApiModelProperty("内容")
    private String content;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("documentId", getDocumentId())
            .append("flowId", getFlowId())
            .append("fieldId", getFieldId())
            .append("content", getContent())
            .toString();
    }
}
