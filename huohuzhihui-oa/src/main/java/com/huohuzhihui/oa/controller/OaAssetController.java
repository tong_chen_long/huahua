package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaAsset;
import com.huohuzhihui.oa.service.IOaAssetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 资产信息Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("资产信息信息管理")
@RestController
@RequestMapping("/oa/asset")
public class OaAssetController extends BaseController
{
    @Autowired
    private IOaAssetService oaAssetService;

    /**
     * 查询资产信息列表
     */
    @ApiOperation("获取资产信息列表")
    @PreAuthorize("@ss.hasPermi('oa:asset:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaAsset oaAsset)
    {
        startPage();
        List<OaAsset> list = oaAssetService.selectOaAssetList(oaAsset);
        return getDataTable(list);
    }

    /**
     * 导出资产信息列表
     */
    @ApiOperation("导出资产信息列表")
    @PreAuthorize("@ss.hasPermi('oa:asset:export')")
    @Log(title = "资产信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaAsset oaAsset)
    {
        List<OaAsset> list = oaAssetService.selectOaAssetList(oaAsset);
        ExcelUtil<OaAsset> util = new ExcelUtil<OaAsset>(OaAsset.class);
        return util.exportExcel(list, "资产数据");
    }

    /**
     * 获取资产信息详细信息
     */
    @ApiOperation("获取资产信息详细信息")
    @ApiImplicitParam(name = "id", value = "资产信息ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:asset:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaAssetService.selectOaAssetById(id));
    }

    /**
     * 新增资产信息
     */
    @ApiOperation("新增资产信息")
    @PreAuthorize("@ss.hasPermi('oa:asset:add')")
    @Log(title = "资产信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaAsset oaAsset)
    {
        return toAjax(oaAssetService.insertOaAsset(oaAsset));
    }

    /**
     * 修改资产信息
     */
    @ApiOperation("修改资产信息")
    @PreAuthorize("@ss.hasPermi('oa:asset:edit')")
    @Log(title = "资产信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaAsset oaAsset)
    {
        return toAjax(oaAssetService.updateOaAsset(oaAsset));
    }

    /**
     * 删除资产信息
     */
    @ApiOperation("删除资产信息")
    @ApiImplicitParam(name = "id", value = "资产信息ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:asset:remove')")
    @Log(title = "资产信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaAssetService.deleteOaAssetByIds(ids));
    }
    
    @Log(title = "导入资产信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('oa:asset:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<OaAsset> util = new ExcelUtil<OaAsset>(OaAsset.class);
        List<OaAsset> list = util.importExcel(file.getInputStream());
        String operName = SecurityUtils.getUsername();
        String message = oaAssetService.imports(list, updateSupport, operName);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate()
    {
        ExcelUtil<OaAsset> util = new ExcelUtil<OaAsset>(OaAsset.class);
        return util.importTemplateExcel("资产数据");
    }
}
