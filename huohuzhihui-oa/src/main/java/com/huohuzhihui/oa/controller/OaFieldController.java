package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaField;
import com.huohuzhihui.oa.service.IOaFieldService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 单字段Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("单字段信息管理")
@RestController
@RequestMapping("/oa/field")
public class OaFieldController extends BaseController
{
    @Autowired
    private IOaFieldService oaFieldService;

    /**
     * 查询单字段列表
     */
    @ApiOperation("获取单字段列表")
    @PreAuthorize("@ss.hasPermi('oa:field:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaField oaField)
    {
        startPage();
        List<OaField> list = oaFieldService.selectOaFieldList(oaField);
        return getDataTable(list);
    }
    

    /**
     * 查询单字段列表
     */
    @ApiOperation("获取单字段列表")
    @GetMapping("/select")
    public TableDataInfo select(OaField oaField)
    {
        List<OaField> list = oaFieldService.selectOaFieldList(oaField);
        return getDataTable(list);
    }

    /**
     * 导出单字段列表
     */
    @ApiOperation("导出单字段列表")
    @PreAuthorize("@ss.hasPermi('oa:field:export')")
    @Log(title = "单字段", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaField oaField)
    {
        List<OaField> list = oaFieldService.selectOaFieldList(oaField);
        ExcelUtil<OaField> util = new ExcelUtil<OaField>(OaField.class);
        return util.exportExcel(list, "field");
    }

    /**
     * 获取单字段详细信息
     */
    @ApiOperation("获取单字段详细信息")
    @ApiImplicitParam(name = "id", value = "单字段ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:field:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaFieldService.selectOaFieldById(id));
    }

    /**
     * 新增单字段
     */
    @ApiOperation("新增单字段")
    @PreAuthorize("@ss.hasPermi('oa:field:add')")
    @Log(title = "单字段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaField oaField)
    {
        return toAjax(oaFieldService.insertOaField(oaField));
    }

    /**
     * 修改单字段
     */
    @ApiOperation("修改单字段")
    @PreAuthorize("@ss.hasPermi('oa:field:edit')")
    @Log(title = "单字段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaField oaField)
    {
        return toAjax(oaFieldService.updateOaField(oaField));
    }

    /**
     * 删除单字段
     */
    @ApiOperation("删除单字段")
    @ApiImplicitParam(name = "id", value = "单字段ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:field:remove')")
    @Log(title = "单字段", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaFieldService.deleteOaFieldByIds(ids));
    }
}
