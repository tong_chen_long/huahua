package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaArchive;
import com.huohuzhihui.oa.domain.OaArchiveRead;
import com.huohuzhihui.oa.mapper.OaArchiveMapper;
import com.huohuzhihui.oa.mapper.OaArchiveReadMapper;
import com.huohuzhihui.oa.service.IOaArchiveReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 轮阅记录Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@Service
public class OaArchiveReadServiceImpl implements IOaArchiveReadService 
{
    @Autowired
    private OaArchiveReadMapper oaArchiveReadMapper;
    @Autowired
    private OaArchiveMapper oaArchiveMapper;

    /**
     * 查询轮阅记录
     * 
     * @param id 轮阅记录ID
     * @return 轮阅记录
     */
    @Override
    public OaArchiveRead selectOaArchiveReadById(Long id)
    {
    	return oaArchiveReadMapper.selectOaArchiveReadById(id);
    }

    /**
     * 查询轮阅记录列表
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 轮阅记录
     */
    @Override
    public List<OaArchiveRead> selectOaArchiveReadList(OaArchiveRead oaArchiveRead)
    {
        return oaArchiveReadMapper.selectOaArchiveReadList(oaArchiveRead);
    }

    /**
     * 新增轮阅记录
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 结果
     */
    @Override
    public int insertOaArchiveRead(OaArchiveRead oaArchiveRead)
    {
        return oaArchiveReadMapper.insertOaArchiveRead(oaArchiveRead);
    }

    /**
     * 修改轮阅记录
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 结果
     */
    @Override
    public int updateOaArchiveRead(OaArchiveRead oaArchiveRead)
    {
        return oaArchiveReadMapper.updateOaArchiveRead(oaArchiveRead);
    }

    /**
     * 批量删除轮阅记录
     * 
     * @param ids 需要删除的轮阅记录ID
     * @return 结果
     */
    @Override
    public int deleteOaArchiveReadByIds(Long[] ids)
    {
        return oaArchiveReadMapper.deleteOaArchiveReadByIds(ids);
    }

    /**
     * 删除轮阅记录信息
     * 
     * @param id 轮阅记录ID
     * @return 结果
     */
    @Override
    public int deleteOaArchiveReadById(Long id)
    {
        return oaArchiveReadMapper.deleteOaArchiveReadById(id);
    }
    
    /**
     * 标记已读
     * 
     * @param ids 需要标记的文档轮阅ID
     * @return 结果
     */
    @Override
    @Transactional
    public int markRead(Long[] ids) {
    	for(Long id:ids) {
    		OaArchiveRead read = oaArchiveReadMapper.selectOaArchiveReadById(id);
    		if(!read.getUserId().equals(SecurityUtils.getLoginUser().getUser().getUserId())) {
    			throw new CustomException("您没有权限!");
    		}
    		if(read.getReadTime() == null) {
    			read.setReadTime(DateUtils.getNowDate());
    			oaArchiveReadMapper.updateOaArchiveRead(read);
    		}
    		
    		OaArchiveRead rc = new OaArchiveRead();
    		rc.setArchiveId(read.getArchiveId());
    		rc.getParams().put("status", "N");
    		List<OaArchiveRead> list = oaArchiveReadMapper.selectOaArchiveReadList(rc);
    		if(list == null || list.size() == 0) {
        		OaArchive archive = oaArchiveMapper.selectOaArchiveById(read.getArchiveId());
        		archive.setStatus("Y");
        		oaArchiveMapper.updateOaArchive(archive);
    		}
    	}
    	return ids.length;
    }
}
