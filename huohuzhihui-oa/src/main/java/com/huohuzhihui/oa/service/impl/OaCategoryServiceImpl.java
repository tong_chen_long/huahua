package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.oa.domain.OaCategory;
import com.huohuzhihui.oa.mapper.OaCategoryMapper;
import com.huohuzhihui.oa.service.IOaCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资产分类Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaCategoryServiceImpl implements IOaCategoryService 
{
    @Autowired
    private OaCategoryMapper oaCategoryMapper;

    /**
     * 查询资产分类
     * 
     * @param id 资产分类ID
     * @return 资产分类
     */
    @Override
    public OaCategory selectOaCategoryById(Long id)
    {
        return oaCategoryMapper.selectOaCategoryById(id);
    }

    /**
     * 查询资产分类列表
     * 
     * @param oaCategory 资产分类
     * @return 资产分类
     */
    @Override
    public List<OaCategory> selectOaCategoryList(OaCategory oaCategory)
    {
        return oaCategoryMapper.selectOaCategoryList(oaCategory);
    }

    /**
     * 新增资产分类
     * 
     * @param oaCategory 资产分类
     * @return 结果
     */
    @Override
    public int insertOaCategory(OaCategory oaCategory)
    {
        return oaCategoryMapper.insertOaCategory(oaCategory);
    }

    /**
     * 修改资产分类
     * 
     * @param oaCategory 资产分类
     * @return 结果
     */
    @Override
    public int updateOaCategory(OaCategory oaCategory)
    {
        return oaCategoryMapper.updateOaCategory(oaCategory);
    }

    /**
     * 批量删除资产分类
     * 
     * @param ids 需要删除的资产分类ID
     * @return 结果
     */
    @Override
    public int deleteOaCategoryByIds(Long[] ids)
    {
        return oaCategoryMapper.deleteOaCategoryByIds(ids);
    }

    /**
     * 删除资产分类信息
     * 
     * @param id 资产分类ID
     * @return 结果
     */
    @Override
    public int deleteOaCategoryById(Long id)
    {
        return oaCategoryMapper.deleteOaCategoryById(id);
    }
}
