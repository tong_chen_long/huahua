package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaCategory;

import java.util.List;

/**
 * 资产分类Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaCategoryService 
{
    /**
     * 查询资产分类
     * 
     * @param id 资产分类ID
     * @return 资产分类
     */
    public OaCategory selectOaCategoryById(Long id);

    /**
     * 查询资产分类列表
     * 
     * @param oaCategory 资产分类
     * @return 资产分类集合
     */
    public List<OaCategory> selectOaCategoryList(OaCategory oaCategory);

    /**
     * 新增资产分类
     * 
     * @param oaCategory 资产分类
     * @return 结果
     */
    public int insertOaCategory(OaCategory oaCategory);

    /**
     * 修改资产分类
     * 
     * @param oaCategory 资产分类
     * @return 结果
     */
    public int updateOaCategory(OaCategory oaCategory);

    /**
     * 批量删除资产分类
     * 
     * @param ids 需要删除的资产分类ID
     * @return 结果
     */
    public int deleteOaCategoryByIds(Long[] ids);

    /**
     * 删除资产分类信息
     * 
     * @param id 资产分类ID
     * @return 结果
     */
    public int deleteOaCategoryById(Long id);
}
