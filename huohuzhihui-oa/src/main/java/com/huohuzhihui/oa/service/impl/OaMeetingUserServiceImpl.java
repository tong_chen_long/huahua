package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.oa.domain.OaMeetingUser;
import com.huohuzhihui.oa.mapper.OaMeetingUserMapper;
import com.huohuzhihui.oa.service.IOaMeetingUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 与会人员Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaMeetingUserServiceImpl implements IOaMeetingUserService 
{
    @Autowired
    private OaMeetingUserMapper oaMeetingUserMapper;

    /**
     * 查询与会人员
     * 
     * @param id 与会人员ID
     * @return 与会人员
     */
    @Override
    public OaMeetingUser selectOaMeetingUserById(Long id)
    {
        return oaMeetingUserMapper.selectOaMeetingUserById(id);
    }

    /**
     * 查询与会人员列表
     * 
     * @param oaMeetingUser 与会人员
     * @return 与会人员
     */
    @Override
    public List<OaMeetingUser> selectOaMeetingUserList(OaMeetingUser oaMeetingUser)
    {
        return oaMeetingUserMapper.selectOaMeetingUserList(oaMeetingUser);
    }

    /**
     * 新增与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    @Override
    public int insertOaMeetingUser(OaMeetingUser oaMeetingUser)
    {
        return oaMeetingUserMapper.insertOaMeetingUser(oaMeetingUser);
    }

    /**
     * 修改与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    @Override
    public int updateOaMeetingUser(OaMeetingUser oaMeetingUser)
    {
        return oaMeetingUserMapper.updateOaMeetingUser(oaMeetingUser);
    }

    /**
     * 批量删除与会人员
     * 
     * @param ids 需要删除的与会人员ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingUserByIds(Long[] ids)
    {
        return oaMeetingUserMapper.deleteOaMeetingUserByIds(ids);
    }

    /**
     * 删除与会人员信息
     * 
     * @param id 与会人员ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingUserById(Long id)
    {
        return oaMeetingUserMapper.deleteOaMeetingUserById(id);
    }
}
