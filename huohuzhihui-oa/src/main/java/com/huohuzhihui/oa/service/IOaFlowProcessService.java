package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaDocument;
import com.huohuzhihui.oa.domain.OaFlowProcess;
import com.huohuzhihui.oa.domain.OaFlowStep;

import java.util.List;
import java.util.Set;

/**
 * 工作流步骤Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaFlowProcessService 
{
    /**
     * 查询工作流步骤
     * 
     * @param id 工作流步骤ID
     * @return 工作流步骤
     */
    public OaFlowProcess selectOaFlowProcessById(Long id);

    /**
     * 查询工作流步骤列表
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 工作流步骤集合
     */
    public List<OaFlowProcess> selectOaFlowProcessList(OaFlowProcess oaFlowProcess);

    /**
     * 新增工作流步骤
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 结果
     */
    public int insertOaFlowProcess(OaFlowProcess oaFlowProcess);

    /**
     * 修改工作流步骤
     * 
     * @param oaFlowProcess 工作流步骤
     * @return 结果
     */
    public int updateOaFlowProcess(OaFlowProcess oaFlowProcess);

    /**
     * 批量删除工作流步骤
     * 
     * @param ids 需要删除的工作流步骤ID
     * @return 结果
     */
    public int deleteOaFlowProcessByIds(Long[] ids);

    /**
     * 删除工作流步骤信息
     * 
     * @param id 工作流步骤ID
     * @return 结果
     */
    public int deleteOaFlowProcessById(Long id);

    
    /**
     * 处理一步流程
     * @param oaDocument OaDocument 公文
     * @param step OaFlowStep 步骤
     * @return
     */
    public int processStep(OaDocument document, OaFlowStep step) ;

    /**
     * 查询当前用户对应指定步骤可操作的用户列表
     * 
     * @param docment OaDocument 公文
     * @param oaFlowStep 工作流步骤
     * @return 结果
     */
    public Set<Long> selectStepUser(OaDocument docment, OaFlowStep oaFlowStep);

    /**
     * 签收工作流
     * 
     * @param id 工作流步骤ID
     * @return 结果
     */
    public int signinOaFlowProcess(Long id);

    
}
