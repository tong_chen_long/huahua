package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaArchive;

import java.util.List;

/**
 * 文档轮阅Service接口
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
public interface IOaArchiveService 
{
    /**
     * 查询文档轮阅
     * 
     * @param id 文档轮阅ID
     * @return 文档轮阅
     */
    public OaArchive selectOaArchiveById(Long id);

    /**
     * 查询文档轮阅列表
     * 
     * @param oaArchive 文档轮阅
     * @return 文档轮阅集合
     */
    public List<OaArchive> selectOaArchiveList(OaArchive oaArchive);

    /**
     * 新增文档轮阅
     * 
     * @param oaArchive 文档轮阅
     * @return 结果
     */
    public int insertOaArchive(OaArchive oaArchive);

    /**
     * 修改文档轮阅
     * 
     * @param oaArchive 文档轮阅
     * @return 结果
     */
    public int updateOaArchive(OaArchive oaArchive);

    /**
     * 批量删除文档轮阅
     * 
     * @param ids 需要删除的文档轮阅ID
     * @return 结果
     */
    public int deleteOaArchiveByIds(Long[] ids);

    /**
     * 删除文档轮阅信息
     * 
     * @param id 文档轮阅ID
     * @return 结果
     */
    public int deleteOaArchiveById(Long id);
    
}
