package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaArchiveRead;

import java.util.List;

/**
 * 轮阅记录Service接口
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
public interface IOaArchiveReadService 
{
    /**
     * 查询轮阅记录
     * 
     * @param id 轮阅记录ID
     * @return 轮阅记录
     */
    public OaArchiveRead selectOaArchiveReadById(Long id);

    /**
     * 查询轮阅记录列表
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 轮阅记录集合
     */
    public List<OaArchiveRead> selectOaArchiveReadList(OaArchiveRead oaArchiveRead);

    /**
     * 新增轮阅记录
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 结果
     */
    public int insertOaArchiveRead(OaArchiveRead oaArchiveRead);

    /**
     * 修改轮阅记录
     * 
     * @param oaArchiveRead 轮阅记录
     * @return 结果
     */
    public int updateOaArchiveRead(OaArchiveRead oaArchiveRead);

    /**
     * 批量删除轮阅记录
     * 
     * @param ids 需要删除的轮阅记录ID
     * @return 结果
     */
    public int deleteOaArchiveReadByIds(Long[] ids);

    /**
     * 删除轮阅记录信息
     * 
     * @param id 轮阅记录ID
     * @return 结果
     */
    public int deleteOaArchiveReadById(Long id);
    
    /**
     * 标记已读
     * 
     * @param ids 需要标记的文档轮阅ID
     * @return 结果
     */
    public int markRead(Long[] ids);
}
